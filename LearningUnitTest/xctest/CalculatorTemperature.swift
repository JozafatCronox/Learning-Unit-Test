//
//  CalculetorTemperature.swift
//  Tests
//
//  Created by Alfonso Garcia Muñoz on 12/05/21.
//

import Foundation

class CalculatorTemperature {
    
    init() {
        print("CalculatorTemperature inizialized")
    }
    
    deinit {
        print("CalculatorTemperature destroyed")
    }
    
    func toFarenheit(degree: Double) -> Double {
        return degree * 9 / 5 + 32
    }
    
    func toDegrees(fahrenheit: Double) -> Double {
        return (fahrenheit - 32) * 5 / 9
    }
}
