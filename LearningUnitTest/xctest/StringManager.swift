//
//  StringManager.swift
//  Tests
//
//  Created by Alfonso Garcia Muñoz on 12/05/21.
//

import Foundation

class StringManager {
    
    init() {
        print("StringManager inizialized")
    }
    
    deinit {
        print("StringManager destroyed")
    }
    
    func toLowerCased(message: String) -> String{
        return message.lowercased()
    }
    
    func toUpperCased(message: String) -> String {
        return message.uppercased()
    }
}
