//
//  AddTests.swift
//  TestsTests
//
//  Created by Alfonso Garcia Muñoz on 12/05/21.
//

import Foundation
import XCTest
import Mockingbird

@testable import LearningUnitTest

class AddTests: XCTestCase {
    var sut: Add?
    
    let mockValidNumber: ValidNumberMock = mock(ValidNumber.self)
    let mockPrint: PrintMock = mock(Print.self)
    
    override func setUp() {
        sut = Add(validNumber: mockValidNumber, print: mockPrint)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testAdd(){
        given(mockValidNumber.check(number: 3)) ~> true
        given(mockValidNumber.check(number: 4)) ~> true
        let _ = sut?.add(a: 3, b: 4)
        verify(mockValidNumber.check(number: 3)).wasCalled()
        verify(mockValidNumber.check(number: 4)).wasCalled()
    }
    
    func testAdd2(){
        given(mockValidNumber.check(number: any())) ~> true
        var checkNumber: Bool =  mockValidNumber.check(number: 30)
        XCTAssertEqual(true, checkNumber)
        
        given(mockValidNumber.check(number: -3)) ~> true
        checkNumber =  mockValidNumber.check(number: -3)
        XCTAssertEqual(true, checkNumber)
    }
    
    func testAAAPAttern(){
        //Arrange Se configura el test, con given
        given(mockValidNumber.check(number: 3)) ~> true
        given(mockValidNumber.check(number: 4)) ~> true
        
        //Act Se realiza la llamada al metodo a probar
        let result = sut?.add(a: 3, b: 4)
        
        //Assert Se valida con un assert
        XCTAssertEqual(7, result)
    }
    
    func testBDDPattern(){
        //Given
        given(mockValidNumber.check(number: 3)) ~> true
        given(mockValidNumber.check(number: 4)) ~> true
        
        //When
        let result = sut?.add(a: 3, b: 4)
        
        //Then
        XCTAssertEqual(7, result)
    }
    
    func testAgumentsMatcher(){
        //Given
        given(mockValidNumber.check(number: any(of: 3,4))) ~> true
        
        //When
        let result = sut?.add(a: 3, b: 4)
        
        //Then
        XCTAssertEqual(7, result)
    }
    
    func testAddPrint(){
        //Given
        given(mockValidNumber.check(number: any())) ~> true
        
        //When
        sut?.addPrint(a: 3, b: 3)
        
        //Then
        
        //verify(mockValidNumber.check(number: 3)).wasCalled()
        verify(mockValidNumber.check(number: 3)).wasCalled(exactly(2))
        verify(mockValidNumber.check(number: 9)).wasNeverCalled()
        verify(mockValidNumber.check(number: 3)).wasCalled(atLeast(1))
        verify(mockValidNumber.check(number: 1)).wasCalled(atMost(3))
        
        
        verify(mockPrint.showMessage(numero: 6)).wasCalled()
    }
    
    func testAddPrintShowError(){
        //Given
        given(mockValidNumber.check(number: any())) ~> false
        
        //When
        sut?.addPrint(a: 3, b: 3)
        
        //then
        verify(mockPrint.showError()).wasCalled()
    }
}
