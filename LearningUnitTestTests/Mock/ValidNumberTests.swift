//
//  ValidNumberTest.swift
//  TestsTests
//
//  Created by Alfonso Garcia Muñoz on 12/05/21.
//

import Foundation
import XCTest

@testable import LearningUnitTest

class ValidNumberTests: XCTestCase {
    var sut: ValidNumber!
    
    override func setUp() {
        sut = ValidNumber()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testCheck(){
        XCTAssertEqual(true, sut.check(number: 4))
    }
    
    func testCheckNegative(){
        XCTAssertEqual(false, sut.check(number: -4))
    }
    
    func testCheckOver(){
        XCTAssertEqual(false, sut.check(number: 14))
    }
}
