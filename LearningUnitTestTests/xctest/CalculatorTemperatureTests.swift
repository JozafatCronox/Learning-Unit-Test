//
//  CalculetorTemperatureTests.swift
//  TestsTests
//
//  Created by Alfonso Garcia Muñoz on 12/05/21.
//

import Foundation
import XCTest

@testable import LearningUnitTest

class CalculatorTemperatureTests : XCTestCase {
    var sut: CalculatorTemperature!
    
    override func setUp() {
        sut = CalculatorTemperature()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testToFarenheit(){
        let expectedResult = -9.4
        
        let result = sut.toFarenheit(degree: -23)
        
        XCTAssertEqual(expectedResult, result, accuracy: 0.01)
    }
    
    func testToDegrees(){
        let expectedResult = 25.55
        
        let result = sut.toDegrees(fahrenheit: 78.0)
        
        XCTAssertEqual(expectedResult, result, accuracy: 0.01)
    }
}
