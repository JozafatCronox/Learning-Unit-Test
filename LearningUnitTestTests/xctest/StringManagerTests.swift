//
//  StringManagerTests.swift
//  LearningUnitTestTests
//
//  Created by Alfonso Garcia Muñoz on 13/05/21.
//

import Foundation
import XCTest
@testable import LearningUnitTest

class StringManagerTest: XCTestCase {
    var sut: StringManager?
    
    override func setUp() {
        sut = StringManager()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testToUpperCased(){
        XCTAssertEqual("JOSAFATFLORESR@GMAIL.COM", sut?.toUpperCased(message: "JosafatFloresr@Gmail.com"))
    }
    
    func testToLowerCased(){
        XCTAssertEqual("josafatfloresr@gmail.com", sut?.toLowerCased(message: "JosafatFloresr@Gmail.com"))
    }
}
