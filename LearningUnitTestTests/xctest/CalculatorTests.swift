//
//  CalculetorTests.swift
//  TestsTests
//
//  Created by Alfonso Garcia Muñoz on 11/05/21.
//

import Foundation
import XCTest

@testable import LearningUnitTest

class ClaculatorTests: XCTestCase {
    
    var sut: Calculator!
    
    override func setUp() {
        sut = Calculator()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func testCalculatorNotNil(){
        XCTAssertNotNil(sut, "SUT debe esta instanciado")
    }
    
    func testAddAssert(){
        //1.- Arrange
        let expectedResult = 30
        let currentResponse: Int
        
        //2.- Action
        currentResponse = sut.add(n1: 10, n2: 20)
        
        //3.- Assert
        XCTAssertEqual(expectedResult, currentResponse)
    }
    
    func testAddBDD(){
        //1.- Given
        let expectedResult = 30
        let currentResponse: Int
        
        //2.- When
        currentResponse = sut.add(n1: 10, n2: 20)
        
        //3.- Then
        XCTAssertEqual(expectedResult, currentResponse)
    }
    
    func testAdd(){
        XCTAssertEqual(30, sut.add(n1: 10, n2: 20))
    }
    
    func testAssertTypes(){
        XCTAssertTrue(1 == 1)
        XCTAssertFalse(1 == 2)
        XCTAssertNil(nil)
        XCTAssertNotNil(sut)
        XCTAssertEqual(30, sut.add(n1: 10, n2: 20))
        XCTAssertEqual("alberto", "alberto")
        XCTAssertGreaterThan(10, 1, "El numero necesta ser mayor")
        
    }
    
    func testSubstract(){
        XCTAssertEqual(4, sut.substract(n1: 11, n2: 7))
    }
    
    func testDivide(){
        XCTAssertEqual(2, sut.divide(n1: 4, n2: 2))
    }
    
    func testDivideByZero(){
        XCTAssertEqual(2, try sut.divideByZero(n1: 4, n2: 2))
        XCTAssertThrowsError(try sut.divideByZero(n1: 4, n2: 0))
    }
    
    func testDisable(){
        XCTAssertThrowsError(try sut.divideByZero(n1: 4, n2: 0))
    }
    
    func testPerformanceExample() throws {
        
        self.measure {
            let _ = sut.divide(n1: 5, n2: 5)
        }
    }
    
    func testArraySquared(){
        //1.- Given
        var numbers: [Int] = [Int]()
        
        for n in 1...4{
            numbers.append(n)
        }
        
        //2.- When
        let numberSquared = sut.arraySquared(numberArray: numbers)
        
        //then
        var spectedResult = [Int]()
        spectedResult.append(1)
        spectedResult.append(4)
        spectedResult.append(9)
        spectedResult.append(16)
        
        XCTAssertEqual(spectedResult, numberSquared)
        
    }
    
    func testArraySquared2(){
        //1.- Given
        var numbers: [Int] = [Int]()
        
        for n in 1...4{
            numbers.append(n)
        }
        
        //2.- When
        let numberSquared = sut.arraySquared2(numberArray: numbers)
        
        //then
        var spectedResult = [Int]()
        spectedResult.append(1)
        spectedResult.append(4)
        spectedResult.append(9)
        spectedResult.append(16)
        
        XCTAssertEqual(spectedResult, numberSquared)
        
    }
    
    func testArrayMesuare(){
        //1.- Given
        var numbers: [Int] = [Int]()
        
        for n in 1...5000000{
            numbers.append(n)
        }
        
        self.measure {
            //2.- When
            let _ = sut.arraySquared(numberArray: numbers)
        }
    }

    func testArrayMesuare2(){
        //1.- Given
        var numbers: [Int] = [Int]()
        
        for n in 1...5000000{
            numbers.append(n)
        }
        
        self.measure {
            //2.- When
            let _ = sut.arraySquared2(numberArray: numbers)
        }
    }
}
